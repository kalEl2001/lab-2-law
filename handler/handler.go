package handler

import (
	"github.com/gin-gonic/gin"
	"strings"
)

type CaesarianCypherRequest struct {
	Message	string	`json:"message" form:"message"`
	Shift 	int		`json:"shift" form:"shift"`
}

type CaesarianCypherForm struct {
	Message	string	`form:"message"`
	Shift 	int		`form:"shift"`
}

type JSONLevenshteinDistanceResult struct {
	Distance	int		`json:"distance" example:"1"`
}

type JSONCaesarianCyphertextResult struct {
	Message		string	`json:"message" example:"Uftu"`
	Shift		int		`json:"shift" example:"1"`
}

type JSONErrorResult struct {
	Message		string `json:"message" example:"Missing parameter(s)"`
}

// GetLevenshteinDist godoc
// @Summary Get Levenshtein of string s and t
// @Description Get Levenshtein of string s and t from the GET parameter
// @ID get-levenshtein-distance
// @Produce json
// @Param s query string true "String s"
// @Param t query string true "String t"
// @Success 200 {object} JSONLevenshteinDistanceResult{distance=int}
// @Failure 400 {object} JSONErrorResult{message=string} Missing s or t string
// @Router /levenshtein [get]
func GetLevenshteinDist(c *gin.Context) {
	q := c.Request.URL.Query()
	s := q["s"]
	t := q["t"]
	if len(s) == 0 || len(t) == 0 {
		c.JSON(400, JSONErrorResult{
			Message: "Missing argument(s)",
		})
		return
	}
	sRune := []rune(s[0])
	tRune := []rune(t[0])
	dist := levenshtein(sRune, tRune)
	c.JSON(200, JSONLevenshteinDistanceResult{
		Distance: dist,
	})
}

// Refs: https://www.golangprograms.com/golang-program-for-implementation-of-levenshtein-distance.html
func levenshtein(str1, str2 []rune) int {
	s1len := len(str1)
	s2len := len(str2)
	column := make([]int, len(str1)+1)

	for y := 1; y <= s1len; y++ {
		column[y] = y
	}
	for x := 1; x <= s2len; x++ {
		column[0] = x
		lastkey := x - 1
		for y := 1; y <= s1len; y++ {
			oldkey := column[y]
			var incr int
			if str1[y-1] != str2[x-1] {
				incr = 1
			}

			column[y] = minimum(column[y]+1, column[y-1]+1, lastkey+incr)
			lastkey = oldkey
		}
	}
	return column[s1len]
}

// Refs: https://www.golangprograms.com/golang-program-for-implementation-of-levenshtein-distance.html
func minimum(a, b, c int) int {
	if a < b {
		if a < c {
			return a
		}
	} else {
		if b < c {
			return b
		}
	}
	return c
}

// GetCaesarianCypertext godoc
// @Summary Get Caesarian Cyphertext of message m shifted s time(s) 
// @Description Get Caesarian Cyphertext of message m shifted s time(s) from JSON or x-www-form-urlencoded
// @ID get-caesarian-cyphertext
// @Accept json
// @Param messageAndShift body CaesarianCypherRequest true "Plain text to be ciphered and shift amount"
// @Accept x-www-form-urlencoded
// @Param messageAndShift formData CaesarianCypherForm true "Plain text to be ciphered and shift amount"
// @Produce json
// @Success 200 {object} JSONCaesarianCyphertextResult{message=string,shift=int}
// @Failure 400 {object} JSONErrorResult{message=string} Missing parameter(s)
// @Router /caesarian [post]
// Refs: https://www.dotnetperls.com/caesar-go
func PostCaesarianCyphertext(c *gin.Context) {
	var req CaesarianCypherRequest
	if err := c.Bind(&req); err != nil {
		c.JSON(400, JSONErrorResult{
			Message: "Missing argument(s)",
		})
	}
	shifted := strings.Map(func(r rune) rune {
        return caesar(r, req.Shift)
    }, req.Message)
	c.JSON(200, JSONCaesarianCyphertextResult{
		Message: shifted,
		Shift: req.Shift,
	})
}

// Refs: https://www.dotnetperls.com/caesar-go
func caesar(r rune, shift int) rune {
    // Shift character by specified number of places.
    // ... If beyond range, shift backward or forward.
    s := int(r) + shift
    if s > 'z' {
        return rune(s - 26)
    } else if s < 'a' {
        return rune(s + 26)
    }
    return rune(s)
}
