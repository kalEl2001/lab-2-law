package main

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	_ "gitlab.com/kalEl2001/lab-2-law/docs"
	"gitlab.com/kalEl2001/lab-2-law/handler"
)

// @title Lab 2 LAW
// @version 1.0
// @description RESTful WebService and Service Contract

// @contact.name Samuel
// @contact.url https://gitlab.com/kalEl2001
// @contact.email samuel92@ui.ac.id

// @host localhost:8000
// @BasePath /api/v1
// @query.collection.format multi

func main() {
	r := gin.Default()

	defaultAPI := r.Group("/api/v1")
	{
		defaultAPI.GET("/levenshtein", handler.GetLevenshteinDist)
		defaultAPI.POST("/caesarian", handler.PostCaesarianCyphertext)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":8000")
}
